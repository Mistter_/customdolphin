package com.customdolphin.entities;

import com.customdolphin.objects.EntityObj;
import com.customdolphin.pathfinders.PathfinderGoalDolphin;
import net.minecraft.server.v1_16_R2.*;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_16_R2.CraftWorld;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityTargetEvent;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.Set;

public class CustomEntityDolphin extends EntityDolphin {

    public CustomEntityDolphin(Location location, Player player) {
        super(EntityTypes.DOLPHIN, ((CraftWorld) location.getWorld()).getHandle());
        this.setPosition(location.getX(), location.getY(), location.getZ());
        this.setGoalTarget(((CraftPlayer) player).getHandle(), EntityTargetEvent.TargetReason.CUSTOM, true);
        EntityObj.get(player).setDolphin(this);
    }

    @Override
    protected void initPathfinder() {
        Set<PathfinderGoalWrapped> goalD = (Set<PathfinderGoalWrapped>) getPrivateField("d", PathfinderGoalSelector.class, goalSelector);
        goalD.clear();
        Map<PathfinderGoal.Type, PathfinderGoalWrapped> goalC = (Map<PathfinderGoal.Type, PathfinderGoalWrapped>) getPrivateField("c", PathfinderGoalSelector.class, goalSelector);
        goalC.clear();
        Set<PathfinderGoalWrapped> goalD2 = (Set<PathfinderGoalWrapped>) getPrivateField("d", PathfinderGoalSelector.class, targetSelector);
        goalD2.clear();
        Map<PathfinderGoal.Type, PathfinderGoalWrapped> goalC2 = (Map<PathfinderGoal.Type, PathfinderGoalWrapped>) getPrivateField("c", PathfinderGoalSelector.class, targetSelector);
        goalC2.clear();

        this.goalSelector.a(2, new PathfinderGoalDolphin(this, 2.5, 16));
    }

    public static Object getPrivateField(String fieldName, Class clazz, Object object) {
        Field field;
        Object o = null;
        try {
            field = clazz.getDeclaredField(fieldName);
            field.setAccessible(true);
            o = field.get(object);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return o;
    }
}
