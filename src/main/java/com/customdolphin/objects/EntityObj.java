package com.customdolphin.objects;

import net.minecraft.server.v1_16_R2.ChatComponentText;
import net.minecraft.server.v1_16_R2.DamageSource;
import net.minecraft.server.v1_16_R2.EntityDolphin;
import org.bukkit.ChatColor;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class EntityObj {

    private Player player;
    private Entity entityProvisory;
    private EntityDolphin dolphin;
    private boolean isRenaming;
    private String name;

    private static HashMap<Player, EntityObj> cache = new HashMap<>();

    public EntityObj(Player player, Entity entityProvisory) {
        this.player = player;
        this.entityProvisory = entityProvisory;
        this.name = "Friend";
        this.isRenaming = false;
    }

    public EntityObj insert() {
        this.cache.put(this.player, this);
        return this;
    }

    public void delete(Player player) {
        entityProvisory.getWorld().spawnEntity(dolphin.getBukkitEntity().getLocation(), EntityType.DOLPHIN);
        dolphin.damageEntity(DamageSource.GENERIC, 100);
        this.cache.remove(player);
    }

    public static EntityObj get(Player player) {
        return cache.get(player);
    }

    public Entity getEntity() {
        return this.entityProvisory;
    }

    public void setEntity(Entity entityProvisory) {
        this.entityProvisory = entityProvisory;
    }

    public EntityDolphin getDolphin() {
        return dolphin;
    }

    public void setDolphin(EntityDolphin dolphin) {
        this.dolphin = dolphin;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        dolphin.setCustomName(new ChatComponentText(ChatColor.AQUA + name));
    }

    public boolean isRenaming() {
        return isRenaming;
    }

    public void setRenaming(boolean renaming) {
        isRenaming = renaming;
    }
}
