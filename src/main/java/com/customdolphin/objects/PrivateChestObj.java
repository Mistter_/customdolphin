package com.customdolphin.objects;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;

public class PrivateChestObj {

    private Player player;
    private ArrayList<ItemStack> itens;

    private static HashMap<Player, PrivateChestObj> cache = new HashMap<>();

    public PrivateChestObj(Player player) {
        this.player = player;
        this.itens = new ArrayList<>();
    }

    public PrivateChestObj insert() {
        this.cache.put(this.player, this);
        return this;
    }

    public void delete(Player player) {
        cache.remove(player);
    }

    public static PrivateChestObj get(Player player) {
        return cache.get(player);
    }

    public ArrayList<ItemStack> getItens() {
        return itens;
    }

    public void addItem(ItemStack item) {
        this.itens.add(item);
    }

    public void setItens(ArrayList<ItemStack> itens) {
        this.itens = itens;
    }
}
