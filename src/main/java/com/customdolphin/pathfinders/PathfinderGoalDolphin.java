package com.customdolphin.pathfinders;

import net.minecraft.server.v1_16_R2.*;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.Player;

import java.util.EnumSet;

public class PathfinderGoalDolphin extends PathfinderGoal {

    private final EntityInsentient a;
    private EntityLiving b;

    private double f;
    private final float g;

    private double c;
    private double d;
    private double e;

    public PathfinderGoalDolphin(EntityInsentient a, double speed, float distance) {
        this.a = a;
        this.f = speed;
        this.g = distance;
        this.a(EnumSet.of(PathfinderGoal.Type.MOVE));
    }

    @Override
    public boolean a() {
        this.b = this.a.getGoalTarget();

        if (this.b.getBukkitEntity().getLocation().distance(this.a.getBukkitEntity().getLocation()) > (this.g * 2)) {
            this.a.teleportAndSync(this.b.locX(), this.b.locY(), this.b.locZ());
            return false;
        } else if (this.b.getBukkitEntity().getLocation().distance(this.a.getBukkitEntity().getLocation()) > this.g) {
            this.a.getNavigation().a(this.b.locX(), this.b.locY(), this.b.locZ(), this.f);
            return false;
        }

        if (this.b == null || this.a == null) {
            return false;
        } else if (this.a.getDisplayName() == null) {
            return false;
        } else if (a.passengers != null && a.passengers.size() >= 1) {
            EntityHuman human = (EntityHuman) a.passengers.get(0);

            float aT = human.aT;
            float aR = human.aR;

            if (aT != 0 || aR != 0) {
                float yaw = human.getBukkitEntity().getLocation().getYaw();
                float pitch = human.getBukkitEntity().getLocation().getPitch();

                double x = this.a.locX();
                double y = this.a.locY();
                double z = this.a.locZ();

                float dHor = 10f;
                float dVert = 2f;

                if (aT < 0) {
                    z -= dHor * Math.cos(Math.toRadians(yaw));
                    x += dHor * Math.sin(Math.toRadians(yaw));
                }
                if (aT > 0) {
                    z += dHor * Math.cos(Math.toRadians(yaw));
                    x -= dHor * Math.sin(Math.toRadians(yaw));
                }
                if (pitch < 0) {
                    y += dVert * Math.cos(Math.toRadians(pitch));
                }
                if (pitch > 0) {
                    y -= dVert * Math.cos(Math.toRadians(pitch));
                }
                if (aR < 0) {
                    x -= dHor * Math.cos(Math.toRadians(yaw));
                    z -= dHor * Math.sin(Math.toRadians(yaw));
                }
                if (aR > 0) {
                    x += dHor * Math.cos(Math.toRadians(yaw));
                    z += dHor * Math.sin(Math.toRadians(yaw));
                }

                Location to = new Location(human.getWorld().getWorld(), x, y, z);

                ((Player) this.b.getBukkitEntity()).spawnParticle(Particle.WATER_BUBBLE, to, 1);

                this.c = to.getX();
                this.d = to.getY();
                this.e = to.getZ();

                return true;
            } else return false;
        } else {
            return false;
        }
    }

    public void c() {
        this.a.getNavigation().a(this.c, this.d, this.e, this.f);
        this.a.h(this.b.yaw);
        this.a.i(this.b.yaw);
    }

    public boolean b() {
        return !this.a.getNavigation().m() && this.b.h(this.a) < (double) (this.g * this.g);
    }

    public void d() {
        this.b = null;
    }
}
