package com.customdolphin.commands;

import com.customdolphin.objects.EntityObj;
import com.customdolphin.objects.PrivateChestObj;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class CommandDismiss implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;

        if (command.getName().equalsIgnoreCase("dismiss")) {
            if (EntityObj.get(player) != null) {
                for (ItemStack item : PrivateChestObj.get(player).getItens()) {
                    EntityObj.get(player).getDolphin().getBukkitEntity().getWorld().dropItemNaturally(EntityObj.get(player).getDolphin().getBukkitEntity().getLocation(), item);
                }
                PrivateChestObj.get(player).delete(player);
                EntityObj.get(player).delete(player);

                player.sendMessage(ChatColor.GREEN + " - The dolphin was released!");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                player.playSound(player.getLocation(), Sound.ENTITY_DOLPHIN_SWIM, 1.0f, 1.0f);
            } else {
                player.sendMessage(ChatColor.RED + " - Oops, you don't have a dolphin friend ='(");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
            }
        }
        return false;
    }
}
