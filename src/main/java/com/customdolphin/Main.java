package com.customdolphin;

import com.customdolphin.commands.CommandDismiss;
import com.customdolphin.listeners.*;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

    public static Main instance;
    public static Plugin plugin;

    public static Main getInstance() { return Main.instance; }

    @Override
    public void onEnable() {
        Main.instance = this;
        Main.plugin = this;
        Main.instance.getCommand("dismiss").setExecutor(new CommandDismiss());
        Bukkit.getPluginManager().registerEvents(new EntityDismountEvents(), plugin);
        Bukkit.getPluginManager().registerEvents(new PlayerClickEntityEvents(), plugin);
        Bukkit.getPluginManager().registerEvents(new InventoryInteractEvents(), plugin);
        Bukkit.getPluginManager().registerEvents(new ChatEvents(), plugin);
        Bukkit.getPluginManager().registerEvents(new EntityDamageEvents(), plugin);
    }

    @Override
    public void onDisable() {
        //
    }
}
