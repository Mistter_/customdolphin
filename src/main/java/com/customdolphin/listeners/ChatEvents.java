package com.customdolphin.listeners;

import com.customdolphin.objects.EntityObj;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatEvents implements Listener {

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onSpeak(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();

        if (EntityObj.get(player) != null && EntityObj.get(player).isRenaming()) {
            event.setCancelled(true);
            String msg = event.getMessage();

            if (msg.length() > 16) {
                player.sendMessage(ChatColor.RED + " - Oops, this name is so much long, try something less than 16 words.");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
            } else if (msg.length() > 4) {
                EntityObj.get(player).setRenaming(false);
                EntityObj.get(player).setName(msg);

                player.sendMessage(ChatColor.GREEN + " - WoW, this name is amazing!");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                player.playSound(player.getLocation(), Sound.ENTITY_DOLPHIN_SPLASH, 1.0f, 1.0f);
            } else {
                player.sendMessage(ChatColor.RED + " - Oops, this name is so much small, try something bigger than 4 words.");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
            }
        }
    }
}
