package com.customdolphin.listeners;

import com.customdolphin.objects.EntityObj;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class EntityDamageEvents implements Listener {

    @EventHandler
    public void onDeath(EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Player) {
            Player player = (Player) event.getDamager();

            if (EntityObj.get(player) != null && event.getEntity().getCustomName().contains(EntityObj.get(player).getName())) {
                event.setCancelled(true);
            }
        }
    }
}
