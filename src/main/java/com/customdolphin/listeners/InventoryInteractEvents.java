package com.customdolphin.listeners;

import com.customdolphin.Main;
import com.customdolphin.entities.CustomEntityDolphin;
import com.customdolphin.objects.EntityObj;
import com.customdolphin.objects.PrivateChestObj;
import net.minecraft.server.v1_16_R2.ChatComponentText;
import net.minecraft.server.v1_16_R2.WorldServer;
import org.bukkit.*;
import org.bukkit.craftbukkit.v1_16_R2.CraftWorld;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

public class InventoryInteractEvents implements Listener {

    @EventHandler
    public void onInventoryClosed(InventoryCloseEvent event) {
        Player player = (Player) event.getPlayer();

        if (event.getInventory() != null && event.getView().getTitle().equalsIgnoreCase("Private chest: ")) {
            ArrayList<ItemStack> itens = new ArrayList<>();
            for (int i = 0; i < 36; i++) {
                if (event.getInventory().getItem(i) != null && !event.getInventory().getItem(i).getType().equals(Material.AIR)) {
                    itens.add(event.getInventory().getItem(i));
                }
                if (i == 35) {
                    if (PrivateChestObj.get(player) == null) {
                        new PrivateChestObj(player).insert();
                        PrivateChestObj.get(player).setItens(itens);
                    } else PrivateChestObj.get(player).setItens(itens);
                }
            }
        }
    }

    @EventHandler
    public void onInteractInv(final InventoryClickEvent event) {
        final Player player = (Player) event.getWhoClicked();

        //Inventory: Private chest:
        if (event.getClickedInventory() != null && event.getView().getTitle().equalsIgnoreCase("Private chest: ")) {
            if (event.getSlot() >= 36 && event.getSlot() <= 45) event.setCancelled(true);

            if (event.getSlot() == 40) {
                player.closeInventory();
                player.sendMessage(ChatColor.YELLOW + " \n Write the new name of your friend: \n ");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                EntityObj.get(player).setRenaming(true);

                Bukkit.getScheduler().runTaskLaterAsynchronously(Main.plugin, () -> {
                    if (EntityObj.get(player).isRenaming()) {
                        EntityObj.get(player).setRenaming(false);

                        player.sendMessage(ChatColor.RED + " - Oops, you took too long to write the name!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                }, 2400L);
            }
        }

        //Inventory: Hungry!
        if (event.getClickedInventory() != null && event.getView().getTitle().equalsIgnoreCase("Hungry!")) {
            event.setCancelled(true);

            if (event.getSlot() == 13) {
                if (player.getInventory().contains(Material.TROPICAL_FISH)) {
                    player.closeInventory();
                    player.sendMessage(ChatColor.YELLOW + " \n Dolphin: \n" + ChatColor.GRAY + "  - Nhumi! Delicious! \n ");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_DOLPHIN_PLAY, 1.0f, 1.0f);
                    sendParticles(player, EntityObj.get(player).getEntity());

                    ItemStack fish = new ItemStack(Material.TROPICAL_FISH);
                    fish.setAmount(1);
                    player.getInventory().removeItemAnySlot(fish);

                    CustomEntityDolphin dolphin = new CustomEntityDolphin(EntityObj.get(player).getEntity().getLocation(), player);
                    dolphin.setCustomName(new ChatComponentText(ChatColor.AQUA + "Friend"));
                    WorldServer world = ((CraftWorld) player.getWorld()).getHandle();
                    world.addEntity(dolphin);

                    EntityObj.get(player).getEntity().remove();
                } else {
                    player.sendMessage(ChatColor.RED + " - Ops, you don't have fish!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                }
            }
        }
    }

    private static void sendParticles(Player player, Entity entity) {
        final int rotationParticles = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
            double t;
            final double r = 1;

            @Override
            public void run() {
                final Location loc = entity.getLocation();
                this.t = this.t + Math.PI / 8;
                final double x = this.r * Math.cos(this.t);
                final double y = 0.20 * this.t;
                final double z = this.r * Math.sin(this.t);
                loc.add(x, y, z);
                player.spawnParticle(Particle.HEART, loc, 1);
                loc.subtract(x, y, z);
            }
        }, 0L, 1L);

        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
            @Override
            public void run() {
                Bukkit.getScheduler().cancelTask(rotationParticles);
            }
        }, 30L);
    }
}
