package com.customdolphin.listeners;

import com.customdolphin.managers.PrivateChest;
import com.customdolphin.managers.DolphinGiveFood;
import com.customdolphin.objects.EntityObj;
import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;

public class PlayerClickEntityEvents implements Listener {

    @EventHandler
    public void onClick(PlayerInteractEntityEvent event) {
        Player player = event.getPlayer();
        Entity entity = event.getRightClicked();

        if (entity.getType().equals(EntityType.DOLPHIN)) {
            if (player.isSneaking() && entity.getCustomName() != null && entity.getCustomName().contains(ChatColor.AQUA + "")) {
                player.openInventory(PrivateChest.loadInv(player));
            }

            if (entity.getCustomName() != null && entity.getCustomName().contains(ChatColor.AQUA + "")) {
                ((CraftPlayer) player).getHandle().startRiding(EntityObj.get(player).getDolphin());
            } else {
                new EntityObj(player, entity).insert();
                player.openInventory(DolphinGiveFood.loadInv());
            }
        }
    }
}
