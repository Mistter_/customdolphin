package com.customdolphin.managers;

import com.customdolphin.objects.PrivateChestObj;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import java.util.ArrayList;
import java.util.List;

public class PrivateChest {

    private static Inventory inventory = Bukkit.createInventory(null, 5*9, "Private chest: ");

    public static Inventory loadInv(Player player) {
        inventory.clear();

        if (PrivateChestObj.get(player) != null) {
            int a = 0;
            for (ItemStack item : PrivateChestObj.get(player).getItens()) {
                inventory.setItem(a, item);
                a++;
            }
        }

        ItemStack item = new ItemStack(Material.FLOWER_BANNER_PATTERN);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(ChatColor.YELLOW + "Rename");
        List<String> lore = new ArrayList<>();
        lore.add(" ");
        lore.add(ChatColor.GRAY + " Rename your friend name.");
        lore.add(" ");
        meta.setLore(lore);
        item.setItemMeta(removeItemFlags(meta));

        inventory.setItem(40, item);

        return inventory;
    }

    public static ItemMeta removeItemFlags(ItemMeta meta) {
        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        meta.addItemFlags(ItemFlag.HIDE_DESTROYS);
        meta.addItemFlags(ItemFlag.HIDE_DYE);
        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        meta.addItemFlags(ItemFlag.HIDE_PLACED_ON);
        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        return meta;
    }
}
