package com.customdolphin.managers;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class DolphinGiveFood {

    private static Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Hungry!");

    public static Inventory loadInv() {
        ItemStack fish = new ItemStack(Material.TROPICAL_FISH);
        ItemMeta meta = fish.getItemMeta();
        meta.setDisplayName(ChatColor.YELLOW + "Hungry!");
        List<String> lore = new ArrayList<>();
        lore.add(" ");
        lore.add(ChatColor.GRAY + " I'm hungry, i need food!");
        lore.add(ChatColor.GRAY + " give me an apple!");
        lore.add(" ");
        meta.setLore(lore);
        fish.setItemMeta(meta);

        inventory.setItem(13, fish);
        return inventory;
    }
}
